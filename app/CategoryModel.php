<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $table = 'categories';
    public $timestamps = true;

    public function products()
    {
        return $this->hasMany('App\ProductModel', 'id_category', 'id');
    }

    public function categories()
    {
        return $this->belongsTo('App\AllCategoryModel', 'category_id', 'id');
    }
}

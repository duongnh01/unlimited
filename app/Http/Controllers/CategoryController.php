<?php

namespace App\Http\Controllers;

use App\AllCategoryModel;
use App\CategoryModel;
use App\ProductModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $category = CategoryModel::all();
        return view('admin\categories\categories', ['category' => $category]);
    }

    public function addForm()
    {
        $all = AllCategoryModel::all();
        return view('admin\categories\add', ['all' => $all]);
    }

    public function all()
    {
        $all = AllCategoryModel::all();
        return view('admin\categories\allcategory', ['all' => $all]);
    }

    public function filterCate($id)
    {
        $category = CategoryModel::where('category_id', '=', $id)->get();
        return view('admin\categories\categories', ['category' => $category]);
    }

    public function add(Request $request)
    {
        $category = new CategoryModel();
        $this->validate($request,
            [
                'name' => 'required', 'unique:categories,name'
            ],
            [
                'name.required' => 'This field must not be left',
                'name.unique' => 'This names already existed'
            ]);

        $category->name = $request->name;
        $category->category_id = $request->category_id;
        $category->created_at = now();
        $category->save();
        return redirect()->back()->with('notification', 'Created');
    }

    public function editForm($id)
    {
        $category = CategoryModel::find($id);
        $all = AllCategoryModel::all();
        return view('admin\categories\edit', ['category' => $category], ['all' => $all]);
    }

    public function edit($id, Request $request)
    {
        $category = CategoryModel::find($id);
        $this->validate($request, [
            'name' => 'required', 'unique:categories,name'
        ],
            [
                'name.required' => 'This field must not be left',
                'name.unique' => 'This names already existed'
            ]
        );
        $category->name = $request->name;
        $category->category_id = $request->category_id;
        $category->updated_at = now();
        $category->save();
        return redirect()->back()->with('notification', 'Edit Susessfuly');
    }

    public function filterProduct($id)
    {
        $product = ProductModel::where('id_category', '=', $id)->get();
        return view('admin\products\filter', ['product' => $product]);
    }
}

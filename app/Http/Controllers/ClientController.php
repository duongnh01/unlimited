<?php

namespace App\Http\Controllers;

use App\CategoryModel;
use App\EmailModel;
use App\InvoiceDetailModel;
use App\ProductModel;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $category = CategoryModel::all();
        $newProduct = ProductModel::orderBy('id', 'desc')->take(10)->get();
        $hotProduct = ProductModel::withCount('invoice_detail')->orderBy('invoice_detail_count', 'desc')->take(10)->get();
        return view('clients\index', ['category' => $category], ['newProduct' => $newProduct], ['hotProduct' => $hotProduct]);

    }

    public function products($id)
    {
        $product = ProductModel::find($id);

        $sameProduct = ProductModel::where('id_category', '=', $product->id_category)->take(4)->get();

        return view('clients\products', ['product' => $product], ['sameProduct' => $sameProduct]);
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $products = ProductModel::where('name', 'like', '%' . $search . '%')->paginate(4);
        $products->appends(['search' => $search]);
        return view('clients/search', compact('products', 'search'));
    }

    public function store($id)
    {
        $category = CategoryModel::where('category_id', '=', $id)->get();
        return view('clients\store', ['category' => $category]);
    }

    public function aside($id)
    {
        $product = ProductModel::where('id_category', '=', $id)->paginate(5);
        return view('clients\aside', ['product' => $product]);
    }

    public function mailsending(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $email = new EmailModel;
        $email->email = $request->email;
        $email->created_at = now();
        return redirect()->back()->with('notification', 'Subscribe successfully');
    }
}

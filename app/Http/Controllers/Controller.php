<?php

namespace App\Http\Controllers;

use App\AllCategoryModel;
use App\CategoryModel;
use App\ProductModel;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct()
    {
        $category = CategoryModel::all();
        $all = AllCategoryModel::all();
        $hotProduct = ProductModel::withCount('invoice_detail')->orderBy('invoice_detail_count', 'desc')->take(10)->get();
        $newProduct = ProductModel::orderBy('id', 'desc')->take(10)->get();
        $listCart = Cart::content();
        view()->share('category', $category);
        view()->share('hotProduct', $hotProduct);
        view()->share('newProduct', $newProduct);
        view()->share('all', $all);
        view()->share('listCart', $listCart);

    }

}

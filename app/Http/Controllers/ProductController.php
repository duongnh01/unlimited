<?php

namespace App\Http\Controllers;

use App\AllCategoryModel;
use App\CategoryModel;
use App\DetailModel;
use App\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class ProductController extends Controller
{
    public function index()
    {
        $product = ProductModel::paginate(10);
        return view('admin\products\products', ['product' => $product]);
    }

    public function addForm()
    {
        $all = AllCategoryModel::all();
        $category = CategoryModel::all();
        return view('admin\products\add', ['category' => $category], ['all' => $all]);
    }

    public function subcategory($id)
    {
        if ($id != 0) {
            $category = AllCategoryModel::find($id)->allcategory()->select('id', 'name')->get()->toArray();
        } else {
            $category = CategoryModel::all()->toArray();
        }
        return response()->json($category);
    }

    public function add(Request $request)
    {
        $this->validate($request,
            [
                'name' => ' required|unique:products,name',
                'description' => 'required',
                'price' => 'required',
                'image' => 'required',
                'price' => 'numeric',
                'promotion_price' => 'numeric',
            ], [
                'name.required' => 'Name is required',
                'name.unique' => 'This names already existed',
                'description.required' => 'Description is required',
                'price.required' => 'Price is required',
                'image.required' => 'Image is required',
                'price.numeric' => 'This field is number only',
                'promotion_price.numeric' => 'This field is number only'
            ]);
        // dd($request->all());
        $product = new ProductModel;
        //    $detail = new DetailModel;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->id_category = $request->id_category;
        $product->price = $request->price;
        $product->promotion_price = $request->promotion_price;
        // $detail->cpu = $request->cpu;
        // $detail->ram = $request->ram;
        // $detail->display = $request->display;
        // $detail->os = $request->os;
        // $detail->video_card = $request->video_card;
        // $detail->hard_drive = $request->hard_drive;
        // $detail->ports = $request->ports;
        // $detail->created_at = now();
        $product->created_at = now();
        $product->is_active = 1;

        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'gif' && $duoi != 'jpeg') {
                return redirect('admin/products/addForm')->with('notification', 'bạn chỉ được chọn file đuôi jpg,gif,png,jpeg . ');
            }
            $name = $file->getClientOriginalName();
            $Hinh = rand(100, 999) . '-' . $name;
            while (file_exists("upload/products/" . $Hinh)) {
                $Hinh = rand(100, 999) . '-' . $name;

            }
            $file->move("upload/products", $Hinh);
            $product->image = $Hinh;
            if ($request->promotion_price >= $request->price) {
                return redirect()->back()->with('notification', 'Promotion Price Must Be Less Than Original Price');
            } else {
                $product->save();
            }

            return redirect()->back()->with('notification', 'Created');
        }
    }

    public function editForm($id)
    {

        $product = ProductModel::find($id);

        return view('admin\products\edit', ['product' => $product]);
    }

    public function edit($id, Request $request)
    {
        $product = ProductModel::find($id);

        $this->validate($request,
            [
                'name' => ' required',
                'description' => 'required',
                'price' => 'required',
                'price' => 'numeric',
                'promotion_price' => 'numeric',
            ], [
                'name.required' => 'Name is required',
                'description.required' => 'Description is required',
                'price.required' => 'Price is required',
                'price.numeric' => 'This field is number only',
                'promotion_price.numeric' => 'This field is number only'
            ]);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->id_category = $request->id_category;
        $product->price = $request->price;
        $product->promotion_price = $request->promotion_price;
        $product->created_at = now();
        $product->is_active = 1;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'gif' && $duoi != 'jpeg') {
                return redirect('admin/products/add')->with('notification', 'bạn chỉ được chọn file đuôi jpg,gif,png,jpeg . ');
            }
            $name = $file->getClientOriginalName();
            $Hinh = rand(100, 999) . '-' . $name;
            while (file_exists("upload/products/" . $Hinh)) //bat loi chùng anh
            {
                $Hinh = rand(100, 999) . '-' . $name;
            }
            $file->move("upload/products", $Hinh);
            $product->image = $Hinh;
            $product->save();
            return redirect()->back()->with('notification', 'Updated');
        } else {
            $request->image = $product->image;

            $product->save();
            return redirect()->back()->with('notification', 'Updated');
        }


    }

    public function hide($id)
    {
        $product = ProductModel::find($id);
        if ($product->is_active === 1) {
            $product->is_active = 2;
            $product->save();
        } else {
            $product->is_active = 1;
            $product->save();
        }
        return redirect()->back()->with('notification', 'Success');
    }

}

@extends('layouts\app')
@section('content')
    <div class="container">

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="/admin/categories/add" method="POST">
                @csrf
                @if(session('notification'))
                    <div class="alert alert-danger">
                        {{ session('notification') }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="exampleInputEmail1">Brand</label>
                        <input type="text" class="form-control" id="exampleInputEmail1"
                               placeholder="Enter Category Name" name="name">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group ">
                        <label for="my-select">Category</label>
                        <select id="my-select" class="form-control" name="category_id">
                            @foreach($all as $i)
                                <option value="{{$i->id}}">{{$i->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>
    </div>
@endsection

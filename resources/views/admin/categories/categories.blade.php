@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Brand</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <a href="/admin/categories/addForm" class="btn btn-default">Add Brand</a>
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach ($category as $item)
                    <tr>
                        <td>{{$i++}}</td>
                        <td><a href="/admin/categories/filter/{{ $item->id }}"> {{ $item->name   }}</a></td>
                        <td><a href="/admin/categories/editForm/{{$item->id}}">Edit</a></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

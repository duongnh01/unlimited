@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Category</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Quantity</th>
                    <th>Product</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach ($detail as $item)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{$item->detailProduct->name}}</td>
                        <td>{{number_format($item->price*$item->quantity)}}đ</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $detail->links() !!}
        </div>
        <!-- /.card-body -->
    </div>
@endsection

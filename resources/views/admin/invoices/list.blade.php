@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Category</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Total</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach ($invoice as $item)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$item->invoice_email}}</td>
                        <td>{{$item->total}}đ</td>
                        <td>{{$item->created_at}}</td>
                        <td><a href="/admin/detail/{{ $item->id }}">Detail</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $invoice->links() !!}
        </div>
        <!-- /.card-body -->
    </div>
@endsection

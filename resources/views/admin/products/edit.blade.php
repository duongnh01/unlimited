@extends('layouts\app')
@section('content')
    <div class="container">

        <div class="row" style="width:100%">
            <div class="col-md-6" style="margin:auto">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Product</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @if(session('notification'))
                        <div class="alert alert-success">
                            {{ session('notification') }}
                        </div>
                    @endif
                    <form action="{{ route('products.edit', ['id' => $product->id]) }}" method="POST"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="card-body">
                            <div class="form-group ">
                                <label for="exampleInputEmail1">Product Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="name"
                                       value="{{ $product->name }}">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Description</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="description"
                                       value="{{ $product->description }}">

                            </div>
                            <div class="form-group ">
                                <label for="exampleInputPassword1">Price</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="price"
                                       value="{{ $product->price }}">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Promotion Price</label>
                                <input type="text" class="form-control" id="exampleInputPassword1"
                                       name="promotion_price" value="{{ $product->promotion_price }}">

                            </div>
                            <div class="form-group ">
                                <label for="my-select">Category</label>
                                <select id="my-select" class="form-control" name="category_id">
                                    <option
                                        value="{{ $product->category->categories->id }}">{{$product->category->categories->name}}</option>
                                    @foreach($all as $i)
                                        <option value="{{$i->id}}">{{$i->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="my-select">Category</label>
                                <select id="my-select" class="form-control" name="id_category">
                                    <option value="{{ $product->category->id }}">{{$product->category->name}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Image</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" id="exampleInputFile" name="image">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            $('select[name=category_id]').on('change', function () {
                var selected = $(this).find(":selected").attr('value');
                var base_url = 'http://127.0.0.1:8000/admin/products';
                $.ajax({
                    url: base_url + '/category/' + selected + '/subcategory/',
                    type: 'GET',
                    dataType: 'json',

                }).done(function (data) {
                    var select = $('select[name=id_category]');
                    select.empty();
                    select.append('<option value="0">Select Sub Category</option>');
                    $.each(data, function (key, value) {
                        select.append('<option value=' + value.id + '>' + value.name + '</option>');
                    });

                })
            });
        });

    </script>

@endsection

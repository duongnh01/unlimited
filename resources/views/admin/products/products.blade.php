@extends('layouts\app')
@section('content')
    <base href="{{asset('')}}">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Products</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <a href="/admin/products/addForm" class="btn btn-default">Add Product</a>
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Brand</th>
                    <th>IMAGES</th>
                    <th>Price</th>
                    <th>Promotion_Price</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach ($product as $item)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{ $item->name }} </td>
                        <td>{{$item->category->name}}</td>
                        <td><img src="/upload/products/{{$item->image}}" alt="" width="50px;"></td>
                        <td>{{number_format($item->price)}}</td>
                        <td>{{$item->promotion_price}}</td>
                        <td><a href="/admin/products/editForm/{{$item->id}}">Edit</a></td>
                        @if($item->is_active === 1)
                            <td><a onclick="return window.confirm('Are you sure?');"
                                   href="admin/products/hide/{{$item->id}}">Hide</a></td>
                        @else
                            <td><a onclick="return window.confirm('Are you sure?');"
                                   href="admin/products/hide/{{$item->id}}">Show</a></td>
                        @endif
                    </tr>
                @endforeach

                </tbody>

            </table>
            {!! $product->links() !!}
        </div>

        <!-- /.card-body -->


    </div>
@endsection

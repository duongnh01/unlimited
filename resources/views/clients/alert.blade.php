@extends('welcome')
@section('content')
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <div class="col-md-12">
                    <!-- Billing Details -->
                    <div class="billing-details">

                        <div class="section-title">
                            <h3 class="title">Please check your email!!</h3>
                            <p>@if(Auth::user()){{Auth::user()->name}}@else{{$checkout->name}}@endif</p>
                            <p>@if(Auth::user()){{Auth::user()->email}}@else{{$checkout->email}}@endif</p>
                            <p>@if(Auth::user()){{Auth::user()->phone_number}}@else{{$checkout->phone_number}}@endif</p>
                            <p>@if(Auth::user()){{Auth::user()->address}}@else{{$checkout->address}}@endif</p>
                        </div>
                        <!-- /Billing Details -->
                    </div>

                    <!-- Order Details -->
                    <div class="col-md-12 order-details">
                        <div class="section-title text-center">
                            <h3 class="title">Your Order</h3>
                        </div>
                        <div class="order-summary">
                            <div class="order-col">
                                <div><strong>PRODUCT</strong></div>
                                <div><strong>TOTAL</strong></div>
                            </div>
                            @foreach(Cart::content() as $item)
                                <div class="order-products">
                                    <div class="order-col">
                                        <div>{{$item->qty}}x {{$item->name}}</div>
                                        <div><a href="/cart/delete/{{$item->rowId}}">Delete</a></div>
                                        <div>{{number_format($item->price*$item->qty)}}</div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="order-col">
                                <div><strong>TOTAL</strong></div>
                                <div><strong class="order-total">{{$total}}</strong></div>
                            </div>
                        </div>
                        <a href="/cart/destroy" class="primary-btn order-submit">Confirm</a>
                    </div>
                    <!-- /Order Details -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
@endsection

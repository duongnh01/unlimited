@extends('welcome')
@section('content')
    <div class="section">
        <div class="container">
            <div class="row">
                <form action="/cart/insertDetail" method="POST">
                    @csrf
                    @if(session('notification'))
                        <div class="alert alert-danger">
                            {{ session('notification') }}
                        </div>
                    @endif
                    <div class="col-md-7">
                        <div class="billing-details">
                            <div class="section-title">
                                <h3 class="title">Billing address</h3>
                            </div>
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <input class="input" type="text" name="name" placeholder="Full Name"
                                       @if(Auth::user())value="{{Auth::user()->name}}"@endif>
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('email') ?'has-error':''}}">
                                <input class="input" type="email" name="email" placeholder="Email"
                                       @if(Auth::user())value="{{Auth::user()->email}}"@endif>
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group {{$errors->has('address')?'has-error':''}}">
                                <input class="input" type="text" name="address" placeholder="Address"
                                       @if(Auth::user())value="{{Auth::user()->address}}"@endif>
                                <span class="text-danger">{{$errors->first('address')}}</span>
                            </div>
                            <div class="form-group {{$errors->has('phone_number')?'has-errors':''}}">
                                <input class="input" type="text" name="phone_number" placeholder="Telephone"
                                       @if(Auth::user())value="{{Auth::user()->phone_number}}"@endif>
                                <span class="text-danger">{{$errors->first('phone_number')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 order-details">
                        <div class="section-title text-center">
                            <h3 class="title">Your Order</h3>
                        </div>
                        <div class="order-summary">
                            <div class="order-col">
                                <div><strong>PRODUCT</strong></div>
                                <div><strong>TOTAL</strong></div>
                            </div>
                            @foreach($items as $item)
                                <div class="order-products">
                                    <div class="order-col">
                                        @if($item->promotion_price > 0)
                                            <div class="product-label">
                                <span
                                    class="sale">{{number_format(100- $item->promotion_price/$item->price*100)}}%</span>
                                            </div>
                                        @endif
                                        <div>{{$item->qty}}x {{$item->name}}</div>
                                        <div><a onclick="return window.confirm('Are you sure?');" href="/cart/delete/{{$item->rowId}}">Delete</a></div>
                                        <div>{{number_format($item->price*$item->qty)}}đ</div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="order-col">
                                <div><strong>TOTAL</strong></div>
                                <div><strong class="order-total">{{$total}}đ</strong></div>
                            </div>
                        </div>
                        <button type="submit" class="primary-btn order-submit">Place order</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

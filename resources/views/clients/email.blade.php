<div style="width: 50%; margin: auto; border: 1px solid #eeeeee">
    <div class="section-title text-center">
        <h3 class="title">Your Order</h3>
    </div>
    <div class="order-summary" style=" height:auto; padding-top: 50px; text-align: center;">
        <div class="order-col" style="display:flex;justify-content:space-around ">
            <div><strong>PRODUCT</strong></div>
            <div><strong>PRICE</strong></div>
        </div>
        @foreach($content as $item)
            <div class="order-products">
                <div class="order-col" style="display:flex;justify-content:space-around">
                    <div>{{$item->qty}}x {{$item->name}}</div>
                    <div>{{number_format($item->price*$item->qty)}}</div>
                </div>
            </div>
        @endforeach
        <div class="order-col">
            <div><strong>TOTAL</strong></div>
            <div><strong class="order-total">{{$total}}</strong></div>
        </div>
    </div>

</div>

@extends('welcome')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Your Email: {{$email}}</h3>
            </div>

            <!-- /.card-header -->
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Your name</th>
                        <th>Your address</th>
                        <th>Your Phone Number</th>
                        <th>Total</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach ($invoice as $item)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->address}}</td>
                            <td>{{$item->phone_number}}</td>
                            <td>{{$item->total}}</td>
                            <td>{{$item->created_at}}</td>
                            <td><a href="/invoicedetail/{{ $item->id }}">Detail</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $invoice->links() !!}
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection
